<a name="1.3.1"></a>
## [1.3.1](https://gitlab.com/FilipStenbeck/geek-api/compare/v1.3.0...v1.3.1) (2019-06-25)


### Bug Fixes

* svae first ([d8621ad](https://gitlab.com/FilipStenbeck/geek-api/commit/d8621ad))
* update stuff ([40daa77](https://gitlab.com/FilipStenbeck/geek-api/commit/40daa77))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/FilipStenbeck/geek-api/compare/v1.2.2...v1.3.0) (2019-06-25)


### Features

* update readme ([ff40a98](https://gitlab.com/FilipStenbeck/geek-api/commit/ff40a98))



<a name="1.2.2"></a>
## [1.2.2](https://gitlab.com/FilipStenbeck/geek-api/compare/v1.2.1...v1.2.2) (2019-06-25)


### Bug Fixes

* add corp semantic release ([7502e85](https://gitlab.com/FilipStenbeck/geek-api/commit/7502e85))



